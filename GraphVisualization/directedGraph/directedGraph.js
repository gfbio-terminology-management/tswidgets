var VIS = {
	
	nodes : [],
	links : [],
	path : 0,
	node : 0,
	tempnodes : [],
	
	start : function () {
	// get the data
		$.getJSON("http://localhost:8080/GFBioRESTfulWS/terminologies/CHEBI/hierarchicalallbroader",
				{uri:"http://purl.obolibrary.org/obo/CHEBI_33672"}, function(data) {
		$.each(data.results, function() {
			console.log(data.results.toSource());
			var sourceNode = this.uri;
			var node = {
				uri : this.uri,
				label : this.label
			};
			VIS.tempnodes.push(node);
			
			$.each(this.hierarchy, function() {
				VIS.links.push({
					source : sourceNode,
					target : this,
					weight : Math.random(),
				})
			});
		});

	
		// Compute the distinct nodes from the links.
		VIS.links.forEach(function(link) {
		    link.source = VIS.nodes[link.source] || 
		        (VIS.nodes[link.source] = {name: link.source});
		    link.target = VIS.nodes[link.target] || 
		        (VIS.nodes[link.target] = {name: link.target});
		    link.value = +link.value;
		});
		
//		VIS.nodes.forEach(function(node) {
			VIS.tempnodes.forEach(function(tempnode){
				if (VIS.nodes[tempnode.uri].name == tempnode.uri){
					VIS.nodes[tempnode.uri].label= tempnode.label;
				}
			});
//		});
		
		var width = 960,
		    height = 500;
		
		var force = d3.layout.force()
		    .nodes(d3.values(VIS.nodes))
		    .links(VIS.links)
		    .size([width, height])
		    .linkDistance(60)
		    .charge(-300)
		    .on("tick", tick)
		    .start();
		
		var svg = d3.select("body").append("svg")
		    .attr("width", width)
		    .attr("height", height);
		
		// build the arrow.
		svg.append("svg:defs").selectAll("marker")
		    .data(["end"])
		  .enter().append("svg:marker")
		    .attr("id", String)
		    .attr("viewBox", "0 -5 10 10")
		    .attr("refX", 15)
		    .attr("refY", -1.5)
		    .attr("markerWidth", 6)
		    .attr("markerHeight", 6)
		    .attr("orient", "auto")
		  .append("svg:path")
		    .attr("d", "M0,-5L10,0L0,5");
		
		// add the links and the arrows
		path = svg.append("svg:g").selectAll("path")
		    .data(force.links())
		  .enter().append("svg:path")
		    .attr("class", "link")
		    .attr("marker-end", "url(#end)");
		
		// define the nodes
		node = svg.selectAll(".node")
		    .data(force.nodes())
		  .enter().append("g")
		    .attr("class", "node")
		    .call(force.drag);
		
		// add the nodes
		node.append("circle")
		    .attr("r", 5);
		
		var div = d3.select("body").append("div").attr("class", "tooltip").style("opacity", 0);

		
		svg.selectAll("circle").on("mouseover", function(d) {      
	    	div.transition().duration(200).style("opacity", .9);
	        if (d.uri){
	            div .html(d.uri)  
	                .style("left", (d3.event.pageX) + "px")     
	                .style("top", (d3.event.pageY - 28) + "px");    
	        }
		})                  
	    	.on("mouseout", function(d) {       
	    		div.transition()        
	                .duration(500)      
	                .style("opacity", 0);   
	        });
		
		// add the text 
		node.append("text")
		    .attr("x", 12)
		    .attr("dy", ".35em")
		    .text(function(d) { return d.label; });
		
		});
	
	
	// add the curvy lines
	function tick() {
	    path.attr("d", function(d) {
	        var dx = d.target.x - d.source.x,
	            dy = d.target.y - d.source.y,
	            dr = Math.sqrt(dx * dx + dy * dy);
	        return "M" + 
	            d.source.x + "," + 
	            d.source.y + "A" + 
	            dr + "," + dr + " 0 0,1 " + 
	            d.target.x + "," + 
	            d.target.y;
	    });
	
	    node
	        .attr("transform", function(d) { 
	            return "translate(" + d.x + "," + d.y + ")"; });
	}
	}
};