var VIS = {
	
	nodes : [],
	links : [],
	path : 0,
	node : 0,
	tempnodes : [],
	conceptID : null,
	
	clear: function(){
		d3.select("svg").remove();
		d3.select("div.tooltip").remove();
		VIS.nodes = [];
		VIS.links = [];
		VIS.tempnodes = [];
		VIS.path = 0;
		VIS.node = 0;
		$("#info").html("");
	},
	
	init: function (container) {
		var info = $('<div>').attr('id', 'info');
		var spinnerImg = $('<img>').attr('src', 'http://terminologies.gfbio.org/openSocialGadget/spinner.gif').attr('id',
		'spinner_image');
		var GVis = $('<div>').attr('id','gvis');
		$('#' + container).append(info);
		$('#' + container).append(spinnerImg);
		$('#' + container).append(GVis);
		spinnerImg.hide();
	},
	
	getTermInfo : function (terminologyID, conceptID) {
		$.getJSON("http://terminologies.gfbio.org/api/terminologies/"+terminologyID.toUpperCase()+"/term",
				{uri: conceptID}, function(data) {
			var table = $('<table>');	
			var headlineTerms = $("<h2>").text("Term Details");
			var headlineHierarchy = $("<h2>").text("Top Hierarchy");
			$('#info').append(headlineTerms).append(table).append("<br>").append(headlineHierarchy);
			
			$.each(data.results, function() {
				VIS.buildUpTable(table,this);
			});
			
		});

	},

	buildUpTable : function(table, result) {
		$.each(result, function(key, vale){
		var label = key;
		column = $('<tr>').append(
				$('<td>').text(label))
		if(vale instanceof Array){
			$.each(vale, function(){
				column.append($('<tr style="border: 0;"><td>').text(this));
			})
		}
		else{
			var value = vale;
			column.append($('<td>').text(value));
		}
		
		$(table).append(column);
		})
	},
		// check if attributelabel already in table
		// var text = $('td:contains("'+attributelabel+'")');
		/**if (text.length > 0){
			// if it is already present, group the result values 
			// together in one column
			text.attr('rowspan','1');
			if (attributelabel == "subClassOf"){
				var value = $('<a>').attr('href',result.value).attr('target','_blank')
				.text(result.value);
				var td = $('<tr>').append(value);
				text.next().append(td);
			}
			else {
				var td = $('<tr>').text(result.value);
				text.next().append(td);
			}
		}
		else if (text.length == 0){
			var column;
			if (attributelabel == "subClassOf"){
				var value = $('<a>').attr('href',result.value).attr('target','_blank')
				.text(result.value);
				column = $('<tr>').append(
						$('<td>').text(attributelabel))
						.append($('<td>').append(value));
			}
			else {
			value = result.value
			column = $('<tr>').append(
					$('<td>').text(attributelabel))
					.append($('<td>').text(result.value));
			}
			$(table).append(column);
		}**/
		
	
	start : function (terminologyID, conceptID) {		
		$('#spinner_image').show();
		
		VIS.clear();
		// get the data
		console.log("receiving data");
		VIS.conceptID = conceptID;
		
		VIS.getTermInfo(terminologyID, conceptID);
		var calculation = $.getJSON("http://terminologies.gfbio.org/api/terminologies/"+terminologyID.toUpperCase()+"/hierarchy",
				{uri:conceptID}, function(data) {
		$.each(data.results, function() {
			var sourceNode;
			if (this.uri){
				sourceNode = this.uri;
			}
			else if (this.externalID){
				sourceNode = this.externalID;
			}
	
			var node = {
				uri : this.uri,
				label : this.label,
				rank : this.rank,
				externalID : this.externalID
			};
			VIS.tempnodes.push(node);
			if (data.results.length > 1){
				$.each(this.hierarchy, function() {
					VIS.links.push({
						source : sourceNode,
						target : this,
						weight : Math.random(),
					})
				});
			}
			else {
				// Wenn es nur einen Knoten gibt, möchten wir ihn trotzdem
				// angezeigt bekommen, deswegen verlinken wir ihn mit sich 
				// selbst
				VIS.links.push({
					source : sourceNode,
					target : sourceNode,
					weight : Math.random(),
				})
			}
		});
		$('#spinner_image').hide();

		
		
	
		// Compute the distinct nodes from the links.
		VIS.links.forEach(function(link) {
		    link.source = VIS.nodes[link.source] || 
		        (VIS.nodes[link.source] = {name: link.source});
		    link.target = VIS.nodes[link.target] || 
		        (VIS.nodes[link.target] = {name: link.target});
		    link.value = +link.value;
		});
		
		VIS.tempnodes.forEach(function(tempnode){
			if (tempnode.uri){
				if (VIS.nodes[tempnode.uri].name == tempnode.uri){
					VIS.nodes[tempnode.uri].label= tempnode.label;
					if (tempnode.rank){
						VIS.nodes[tempnode.uri].rank = tempnode.rank
					}
				}
			}
			else if (tempnode.externalID){
				if (VIS.nodes[tempnode.externalID].name == tempnode.externalID){
					VIS.nodes[tempnode.externalID].label = tempnode.label;
					if (tempnode.rank){
						VIS.nodes[tempnode.externalID].rank = tempnode.rank
					}
				}
			}
		});
		
		var width = 700,
		    height = 500;
		
		var force = d3.layout.force()
		    .nodes(d3.values(VIS.nodes))
		    .links(VIS.links)
		    .size([width, height])
		    .linkDistance(60)
		    .charge(-300)
		    .on("tick", tick)
		    .start();
		
		var svg = d3.select("#gvis").append("svg:svg")
		    .attr("width", width)
		    .attr("height", height);
		$("#gvis").hide();
		
		// build the arrow.
		svg.append("svg:defs").selectAll("marker")
		    .data(["end"])
		  .enter().append("svg:marker")
		    .attr("id", String)
		    .attr("viewBox", "0 -5 10 10")
		    .attr("refX", 15)
		    .attr("refY", -1.5)
		    .attr("markerWidth", 6)
		    .attr("markerHeight", 6)
		    .attr("orient", "auto")
		  .append("svg:path")
		    .attr("d", "M0,-5L10,0L0,5");
		
		// add the links and the arrows
		path = svg.append("svg:g").selectAll("path")
		    .data(force.links())
		  .enter().append("svg:path")
		    .attr("class", "link")
		    .attr("marker-end", "url(#end)");
		
		// define the nodes
		node = svg.selectAll(".node")
		    .data(force.nodes())
		  .enter().append("g")
		    .attr("class", "node")
		    .call(force.drag);
		
		// add the nodes
		node.append("circle");
		
		svg.selectAll("circle").style("fill", function(d) { 
			if (d.name == VIS.conceptID){
				return "#39F";
			}
		})
		.attr("r", function(d){
			if (d.name == VIS.conceptID){
				return "8";
			}
			else {
				return "5";
			}
		})                 
		
		var div = d3.select("body").append("div").attr("class", "tooltip").style("opacity", 0);

		
		svg.selectAll("circle").on("mouseover", function(d) {   
	    	div.transition().duration(200).style("opacity", .9);
	        if (d.rank){
	            div .html(d.rank)  
	                .style("left", (d3.event.pageX) + "px")     
	                .style("top", (d3.event.pageY - 28) + "px");    
	        }
	        else if (d.name){
	        	div .html(d.name)  
                .style("left", (d3.event.pageX) + "px")     
                .style("top", (d3.event.pageY - 28) + "px"); 
	        }
		})                  
	    	.on("mouseout", function(d) {       
	    		div.transition()        
	                .duration(500)      
	                .style("opacity", 0);   
	        });
		
		// add the text 
		node.append("text")
		    .attr("x", 12)
		    .attr("dy", ".35em")
		    .text(function(d) { 
		    	return d.label; 
		    });
		
		});
		
		return calculation;
	
		// add the curvy lines
		function tick() {
			console.log("tick");
		    path.attr("d", function(d) {
		        var dx = d.target.x - d.source.x,
		            dy = d.target.y - d.source.y,
		            dr = Math.sqrt(dx * dx + dy * dy);
		        return "M" + 
		            d.source.x + "," + 
		            d.source.y + "A" + 
		            dr + "," + dr + " 0 0,1 " + 
		            d.target.x + "," + 
		            d.target.y;
		    });
		
		    var maxHeight = 0;
			var maxWidth = 0;
				    
		    node
		        .attr("transform", function(d) { 
		            return "translate(" + d.x + "," + d.y + ")"; });
		    
		}
		
	}
	
};