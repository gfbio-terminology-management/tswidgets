var TMS = {
	
	searchDone : function(f) {
		TMS.searchDoneFunc = f;
	},

	cleanUp : function(){
		TMS.dictionary.clear();
		$('#search_results').html($(''));
	},
	
	/***************************************************************************
	 * 
	 * Initialization of container
	 * 
	 **************************************************************************/
	
	init : function(container) {

		var spinnerImg = $('<img>').attr('src', 'http://terminologies.gfbio.org/openSocialGadget/spinner.gif').attr('id',
				'spinner_image');

		var searchResults = $('<div>').attr('id', 'search_results').css('display','none');

		$('#' + container).append(spinnerImg);
		$('#' + container).append(searchResults);
		spinnerImg.hide();

	},

	/***************************************************************************
	 * 
	 * Dictionary
	 * 
	 **************************************************************************/

	dictionary : {
		domains : [],
		getDomain : function(domainName) {
			for (var i = 0; i < TMS.dictionary.domains.length; i++) {
				if (TMS.dictionary.domains[i].name == domainName) {
					return TMS.dictionary.domains[i];
				}
			}
			return null;
		},
		addDomain : function(domainName, results) {
			this.domains.push({
				"name" : domainName,
				"results" : [ results ]
			});
		},
		clear : function() {
			this.domains = [];
		}
	},

	
	
	/***************************************************************************
	 * 
	 * Perform Search
	 * 
	 **************************************************************************/
	performSearch : function(searchquery) {
	
		var restfulws_url = 'http://terminologies.gfbio.org/api/terminologies/search';
		$('#spinner_image').show();
		// delete the recent values from the dictionary
		TMS.dictionary.clear();
		// remove the recent values
		$('#search_results').html($(''));
		$.getJSON(restfulws_url, {
			query : searchquery,
			match_type : "exact"
		}, function(data) {
			if (data) {
				// add data to dictionary
				$.each(data.results, function() {
					if (!this.domain) {
						this.domain = "Uncategorized";
					}

					var domain = TMS.dictionary.getDomain(this.domain);
					if (domain == null) {
						TMS.dictionary.addDomain(this.domain, this);
					} else {
						domain.results.push(this);
					}
				});
			}
			$('#spinner_image').hide();
						
			$('#search_results').show();
			var i = 0;
			$.each(TMS.dictionary.domains, function(index, domain) {
				$.each(this.results, function(index, result) {
					// alert(result.toSource());

					var table = $('<table>');
							
					
					var label_headline = $('<div>').text(result.label + ' - ' + result.sourceTerminology).attr('id','headline');
					
					var uri = $('<a>').attr('id','uri').attr('href',result.uri).attr('target','_blank')
					.text(result.uri);
					
					if (result.uri){
						label_headline.append('<br>').append(uri);
					}
					
					var infobox = $('<div>').attr('id', 'infobox')
					.append(label_headline);
					
					infobox.append('<br>')
					.append(table);
					
					$('#search_results').append(infobox);
					
					TMS.createTable(table, result);
					infobox.append('<br>');
					$('#search_results').append('<br>').append('<br>');
					i++;
					
				});// end of result iteration
			});
		});// end of getJSON
		$('search_results').css('display','inline');
		return 1;
	},

	
	createTable : function(table, result) {

		if (result['label']){
			var label = $('<tr>').append(
					$('<td>').text('Label'))
					.append($('<td>').text(result.label));
			$(table).append(label);
		}
		
		if (result['description']){
			var description = $('<tr>').append($('<td>').text('Description'))
			.append($('<td>').text(result.description));

			$(table).append(description);
		}
		
		if (result['externalID']){
			var externalID = $('<tr>').append(
					$('<td>').text('External ID'))
					.append(
							$('<td>').text(result.externalID));
			$(table).append(externalID);
		}
		
		if (result['rank']){
			var rank = $('<tr>').append(
					$('<td>').text('Rank')).append(
							$('<td>').text(result.rank));
			$(table).append(rank);
		}
		
		if (result['kingdom']){
			var kingdom = $('<tr>')
			.append(
					$('<td>').text('Kingdom')).append(
					$('<td>').text(result.kingdom));
			$(table).append(kingdom);
		}
		
		if (result['status']){
			var status = $('<tr>').append(
					$('<td>').text('Status')).append(
							$('<td>').text(result.status));
			$(table).append(status);
		}
		
	}
	
};
