var TMS = {

	gotEmptyResonse : false,
	maxResults : 10,
	/*
	 * Init fucntion to create an autocomplete search widget.
	 * The widget will use http://terminologies.gfbio.org/api/beta/terminologies/suggest
	 * Param containerID:
	 *  HTML ID of a <input type="text"> element where the widget will be displayed
	 * Param parameters:
	 *  JS Object with these elements:
	 *		-maxResults: number ob maximum results shown in the autocomplete (optional, default 10)
	 *		-URIInputField: ID of a <input> element which will be filled with the uri from the selected entry (optional) 
	 * Example:
	 *  <input type="text" id="aField">
	 *  <input id="uriField">
	 *  createSearch("aField", {maxResults : 15 , URIInputField : 'uriField'})
	 */
	createSearch : function(containerID, parameters){
		if(parameters.maxResults != null){
			TMS.maxResults = parameters.maxResults;	
		}
		//Create a JQuery autocomplete widget
		var a = $('#' + containerID).autocomplete({
			minLength : 3,
			delay : 0,
			source : function(request, response){
				//Perform an ajax request
				var url = 'http://terminologies.gfbio.org/api/beta/terminologies/suggest';
				var requestParams = TMS.createSearchParameters(request.term, parameters);
				$.getJSON(url,requestParams,function(data){
					var items = [];
					//collect all items from ajax request
					$.each(data.results,function(index,item){
						items.push({'label' : TMS.shortTerm(item.label, request.term,70),
							'value' : TMS.shortTerm(item.label, request.term,70),
							'id' : item.uri,
							'query' : request.term
						 });
					});
					//send collected items to the widget
					response(items);
				})
			},
			select : function(event, ui){
				//if user selects an item, fill the URIInputField with the uri from the selected entry
				if(parameters.URIInputField){
			  		$('#' + parameters.URIInputField).val(ui.item.id);
				}
			}
		});

		//redefine the _rendetItem function to highlight the searched query in the entries
		a.data('ui-autocomplete')._renderItem = function(ul, item){
			//insert <b> tags into the label
			var highlightedItem = item.label.replace(item.query,'<b>' + item.query + '</b>');
			return $('<li>').data('item.autocomplete', item)
							.append($('<a>').html(highlightedItem))
							.appendTo(ul);
		};
	},

	//Create a JSON object for ajax request
	createSearchParameters : function(term, parameters){
			return {
				'query' : term,
				'limit' : TMS.maxResults
			};
	},

	/*
	 * Short the given string to 30 characters.
	 * The query will stand in the middl of the shorted string.
	 * The shorted string will be wrapped with 3 dots (...)
	 *
	 * Param logterm:
	 *	Text that will be shorted
	 * Param query:
	 *  Text which will stand in the middl of the shorted string
	 * Param charactersToDisplay:
	 *	The shorted string will be trimmed to this length
	 *
	 * Example:
	 *	var longText = "some text 123HALLO123 some text";
	 *  var shortText = shortTerm(longText, "HALLO",6);
	 *
	 *  shortText will be "...123HALLO123..."
	 */
  	shortTerm : function(longterm, query, charactersToDisplay){
        var index = longterm.indexOf(query);
        var longTermLength = longterm.length;
        var queryLength = query.length;
        var result = query;
        var lowerIndex = index - 1;
        var higherIndex = index + queryLength;

        if(longTermLength <= charactersToDisplay){
          return longterm;
        }

        for(var i = 0; i < charactersToDisplay;){
            if(lowerIndex >= 0){
              result = longterm[lowerIndex] + result;
              lowerIndex--;
              i++;
            }
            if(higherIndex < longTermLength){
              result = result + longterm[higherIndex];
              higherIndex++;
              i++;
            }
        }
        if(lowerIndex > 0)
          result = '...' + result;
        if(higherIndex < longTermLength -1)
          result = result + '...';
        return result;
  }
};
