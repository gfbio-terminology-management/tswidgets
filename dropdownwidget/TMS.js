var TMS = {	
	'createSelect' : function(selectFieldID,parameters){ 
		var restServiceURL = 'http://localhost:8080/GFBioRESTfulWS/terminologies/' + 
							 parameters.terminology;
		var hasRootConcept = (parameters.rootConcept != null && parameters.rootConcept.trim() != '');		
		if(hasRootConcept){
 			restServiceURL += '/allnarrower?uri=' + parameters.rootConcept;
		}else{
			restServiceURL +=  '/allterms';
		}
		console.log(restServiceURL);
		var request = $.getJSON(restServiceURL, function(data){
		var bindings = data.results.bindings;				
		var uriField = '';
		var labelField = '';
		
		if(hasRootConcept){
			uriField = 'narroweruri';
			labelField = 'narrowerlabel';
		}else{
			uriField = 'uri';
			labelField = 'label';
		}

		if(bindings != null){
			$(bindings).each(function(){
			var term = this[labelField].value;
			var uri = this[uriField].value;
			var optionField;

			if(parameters.useURIs != null && parameters.useURIs){
				optionField = $('<option></option>').val(uri).html(term);
			}else{
				optionField = $('<option></option>').val(term).html(term);
			}
			$('#' + selectFieldID).append(optionField);			
		});
			}else{
				console.log('ERROR: json not valid');
			}
		}).fail(function(){
			console.log('ERROR: unable to get json data from service');
		});
		if(parameters.multiselect != null && parameters.multiselect){
			$('#' + selectFieldID).attr('multiple',true);
		}
	}
}
